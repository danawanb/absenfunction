/*! Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *  SPDX-License-Identifier: MIT-0
 */

const AWS = require('aws-sdk')
const s3 = new AWS.S3({apiVersion: '2006-03-01'})
const chromium = require('chrome-aws-lambda')

const pageURL = ('https://edjpb.kemenkeu.go.id/') //bisa juga pake envnya
let usermu = '199706062016121002'
let passwordmu = 'rahasia'
const agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36'

exports.handler = async (event, context) => {

  let result = null
  let browser = null

  try {
    browser = await chromium.puppeteer.launch({
      args: chromium.args,
      defaultViewport: chromium.defaultViewport,
      executablePath: await chromium.executablePath,
      headless: chromium.headless,
      ignoreHTTPSErrors: true,
    });

    let page = await browser.newPage();
    await page.setUserAgent(agent)

    console.log('Navigating to page: ', pageURL)

    await page.goto(pageURL)
    await page.waitForSelector('body > div > div > div > div.d-flex.mg-b-30 > a.btn.btn-md.btn-primary.tx-10.tx-bold.mg-l-3.mg-r-3')
    await page.click('body > div > div > div > div.d-flex.mg-b-30 > a.btn.btn-md.btn-primary.tx-10.tx-bold.mg-l-3.mg-r-3')

    await page.waitForSelector('#username')
    await page.type('#username', usermu) //nip anda
    await page.waitForSelector('#password')
    await page.type('#password', passwordmu) // password anda
    await page.waitForSelector('#tahun')
    await page.select('#tahun','2022')
    await page.waitForTimeout(2000)  
    await page.waitForSelector('#submit')
    await page.click("#submit")
    await page.waitForNavigation({
      waitUntil: "networkidle0",
    })
    await page.waitForSelector('body > div.content.content-fixed > div > div.alert.alert-primary.alert-dismissible.fade.show.tx-12.tx-black.mg-b-5 > a.tx-semibold')
    await page.click('body > div.content.content-fixed > div > div.alert.alert-primary.alert-dismissible.fade.show.tx-12.tx-black.mg-b-5 > a.tx-semibold')
    await page.waitForTimeout(1000)
    //klik clock in
    await page.waitForSelector('body > div.content-body > div > div > div.media-body.mg-t-40.mg-lg-t-2.pd-lg-x-5 > div:nth-child(1) > div > div > div.card-body.tx-center > a')
    await page.click('body > div.content-body > div > div > div.media-body.mg-t-40.mg-lg-t-2.pd-lg-x-5 > div:nth-child(1) > div > div > div.card-body.tx-center > a')
    await page.waitForTimeout(1000)
    
    await page.waitForSelector('#navbarMenu > ul > li:nth-child(2) > a')
    await page.click('#navbarMenu > ul > li:nth-child(2) > a')
    await page.waitForTimeout(1000)
    await page.waitForSelector('body > div.content.content-fixed > div > div.alert.alert-primary.alert-dismissible.fade.show.tx-12.tx-black.mg-b-5 > a:nth-child(3)')
    await page.click('body > div.content.content-fixed > div > div.alert.alert-primary.alert-dismissible.fade.show.tx-12.tx-black.mg-b-5 > a:nth-child(3)')
    //looping kesehatan
    for(let i=11; i<=21; i++) {
        await page.waitForTimeout(100)

        await page.click(`#form > div > div > div > div.card-body.pd-y-10 > fieldset:nth-child(4) > div:nth-child(${i}) > div > div:nth-child(3)`)
    }
    await page.waitForSelector('#form > div > div > div > div.card-footer > div > div > button.btn.btn-sm.ml-auto.btn-warning')
    await page.click('#form > div > div > div > div.card-footer > div > div > button.btn.btn-sm.ml-auto.btn-warning')
    await page.waitForTimeout(2000)
    const buffer = await page.screenshot()
    result = await page.title()

    // upload the image using the current timestamp as filename
    const s3result = await s3
      .upload({
        Bucket: process.env.S3_BUCKET,
        Key: `${Date.now()}.png`,
        Body: buffer,
        ContentType: 'image/png',
        ACL: 'public-read'
      })
      .promise()
      
    console.log('S3 image URL:', s3result.Location)
    
    await page.close()
    await browser.close()
    
  } catch (error) {
    console.log(error)
  } finally {
    if (browser !== null) {
      await browser.close()
    }
  }

  return result
}